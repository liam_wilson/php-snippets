<?php

    $a1 = strtotime("23 October 1982");
    $b1 = mktime(0,0,0, 10, 23, 82);

    echo date("d/M/y", $a1), "<br><br>";
    echo date("d/m/y", $b1), "<br><br>";

    $a2 = strtotime("24 October 1982");
    $b2 = mktime(0,0,0, 10, 24, 82);

    echo date("d/M/y", $a2), "<br><br>";
    echo date("d/m/y", $b2), "<br><br>";

    $a3 = strtotime("25 October 1982");
    $b3 = mktime(0,0,0, 10, 25, 82);

    echo date("d/M/y", $a3), "<br><br>";
    echo date("d/m/y", $b3), "<br><br>";

?>